# Opis projektu

Aplikacja do sterowania otwierania oknem dachowym. Możliwości:
* zdalne zamykanie oraz otwieranie okna do zadanych wartości procentowych,
* możliwość ustawienia automatycznego otwierania okna w przypadku przekroczenia zadanej temperatury w pomieszczeniu i zamykania w przeciwnym przypadku (wykorzystanie czujnika temperatury),
* wysyłanie awaryjnych powiadomień przy podejrzeniu pożaru (oraz ewentualnie przy wykryciu wycieku gazu - użycie kolejnego czujnika).

Ze względu na wysoką cenę silników mogących obsługiwać okno, stworzona zostałaby mini-makieta obrazująca działanie zestawu.
