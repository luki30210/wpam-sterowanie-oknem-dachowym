#pragma once

#define IN1 9
#define IN2 8
#define MOTOR_SPEED A0

/* 
 *  IN1=1, IN2=0 -> LEFT
 *  IN1=0, IN2=1 -> RIGHT
 *  IN1=0, IN2=0 -> STOP
 *  IN1=1, IN2=1 -> STOP
 */

void motorInit() {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(MOTOR_SPEED, OUTPUT);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  analogWrite(MOTOR_SPEED, 0);
}

bool state = false;
int counter = 0, i = 0;
bool komutacja1[12] = { true, false, false, false, false, false, false, false, false, false, false, false };

/*
 * direction = 1 FOR LEFT and ANY for right
 */
void runMotor(char direction, bool state) {
  if (!state) {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    return;
  }

  if (counter > 100) {
    if (direction == 1) {
      digitalWrite(IN1, komutacja1[i]);
      digitalWrite(IN2, LOW);
    } else {
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, komutacja1[i]);
    }
    
    counter = 0;
    if (i < 12) {
      i++;
    } else {
      i = 0;
    }
  } else {
    counter++;
  }
}
