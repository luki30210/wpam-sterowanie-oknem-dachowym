#include "ArduinoJson-v5.13.1.h"
#include "Timers.h"
#include <SoftwareSerial.h>
#include "motor.h"

#define FULL_WINDOW_OPEN_TIME 800

#define LED4 4
#define LED5 5
#define LED6 6
#define LED7 7

bool motorState = false;
Timer timer;
int currentPercentage = 0;
char motorDirection = 2;

SoftwareSerial clientESP(2, 3);  // RX, TX

void setup() {
  Serial.begin(9600);
  clientESP.begin(9600);

  motorInit();
  pinMode(LED4, OUTPUT);
  digitalWrite(LED4, LOW);
  pinMode(LED5, OUTPUT);
  digitalWrite(LED5, LOW);
  pinMode(LED6, OUTPUT);
  digitalWrite(LED6, LOW);
  pinMode(LED7, OUTPUT);
  digitalWrite(LED7, LOW);
}

void loop() {

  if (clientESP.available() > 0) {
    String readedData = clientESP.readString();
    takeAction(readedData);
  }

  runMotor(motorDirection, motorState);

  if (timer.available()) {
    motorState = false;
  }
}

void takeAction(String data) {
  Serial.print(data);

  DynamicJsonBuffer jsonBuffer(200);
  JsonObject& dataJson = jsonBuffer.parseObject(data);

  boolean homeFragment = windowOpenClose(dataJson["percentage"]);
  if (homeFragment) {
    String resp = "{\"response\": \"OK1\"}";
    clientESP.print(resp);
    return;
  }

  String getPercentage = dataJson["request"];
  if (getPercentage.equals("percentage")) {
    String resp = "{\"percentage\": \"";
    resp = resp + currentPercentage;
    resp = resp + "\"}";
    clientESP.print(resp);
    return;
  }

  String autoClose = dataJson["autoClose"];
  if (autoClose.length() > 0) {
    orders(dataJson["autoClose"], dataJson["autoOpen"], dataJson["minTemp"], dataJson["maxTemp"]);
    String resp = "{\"response\": \"OK2\"}";
    clientESP.print(resp);
  }
}

boolean windowOpenClose(String percentageString) {
  if (percentageString.length() < 1) {
    return false;
  }
  
  int newPercentage = percentageString.toInt();
  int difference = newPercentage - currentPercentage;
  currentPercentage = newPercentage;

  if (difference == 0) {
    return false;
  } else if (difference < 0) {
    motorDirection = 1;
    difference = -difference;
  } else {
    motorDirection = 2;
  }

  timer.begin((FULL_WINDOW_OPEN_TIME / 100) * difference);
  motorState = true;
  return true;
}

void orders(String autoClose, String autoOpen, String minTemp, String maxTemp) {
  autoClose.equals("true") ? digitalWrite(LED4, HIGH) : digitalWrite(LED4, LOW);
  autoOpen.equals("true") ? digitalWrite(LED5, HIGH) : digitalWrite(LED5, LOW);
  minTemp.toInt() > 15 ? digitalWrite(LED6, HIGH) : digitalWrite(LED6, LOW);
  maxTemp.toInt() > 26 ? digitalWrite(LED7, HIGH) : digitalWrite(LED7, LOW);
}

