package pl.patro.lukasz.sterowanieoknem.model

class Orders(
        var autoClose: Boolean = false,
        var autoOpen: Boolean = false,
        var minTemp: Int = 11,
        var maxTemp: Int = 22
)