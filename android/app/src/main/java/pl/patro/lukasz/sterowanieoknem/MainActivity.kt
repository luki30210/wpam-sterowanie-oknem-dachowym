package pl.patro.lukasz.sterowanieoknem

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import pl.patro.lukasz.sterowanieoknem.`interface`.AfterButtonSendClickedInterface

class MainActivity : AppCompatActivity(), HomeFragment.OnOpenProgressChangedListener, OrdersFragment.OnOrdersChangedListener {

    var previousFragmentId: Int = 0

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        if (previousFragmentId == item.itemId) {
            return@OnNavigationItemSelectedListener false
        } else {
            buttonSend.visibility = View.INVISIBLE
            previousFragmentId = item.itemId
        }
        when (item.itemId) {
            R.id.navigation_home -> {
                val homeFragment = HomeFragment.newInstance()
                openFragment(homeFragment, "HOME_FRAGMENT")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_orders -> {
                val ordersFragment = OrdersFragment.newInstance()
                openFragment(ordersFragment, "ORDERS_FRAGMENT")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                val settingsFragment = SettingsFragment.newInstance()
                openFragment(settingsFragment, "SETTINGS_FRAGMENT")
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment, tag: String) {
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, tag)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        return
        /*super.onBackPressed()
        println("liczba fragmentów :" + supportFragmentManager.fragments.count())
        val frag = supportFragmentManager.fragments.first() ?: return
        when (frag.tag) {
            "HOME_FRAGMENT" -> {
                navigation.selectedItemId = R.id.navigation_home
            }
            "ORDERS_FRAGMENT" -> {
                navigation.selectedItemId = R.id.navigation_orders
            }
            "SETTINGS_FRAGMENT" -> {
                navigation.selectedItemId = R.id.navigation_settings
            }
        }*/
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        buttonSend.setOnClickListener(buttonSendOnClick)

        val homeFragment = HomeFragment.newInstance()
        openFragment(homeFragment, "HOME_FRAGMENT")
    }

    var openValue: Int = 0
    override fun onOpenProgressChanged(isNew: Boolean, value: Int) {
        if (isNew) {
            buttonSend.visibility = View.VISIBLE
        } else {
            buttonSend.visibility = View.INVISIBLE
        }
        openValue = value
    }

    private val buttonSendOnClick = View.OnClickListener {
        var fragmentABSC = fragmentManager.findFragmentByTag("HOME_FRAGMENT") as AfterButtonSendClickedInterface

        if (fragmentABSC == null || !(fragmentABSC as Fragment).isVisible) {
            fragmentABSC = fragmentManager.findFragmentByTag("ORDERS_FRAGMENT") as AfterButtonSendClickedInterface
        }

        if (fragmentABSC != null && (fragmentABSC as Fragment).isVisible) {
            fragmentABSC.afterButtonSendClicked()
        }
    }

    override fun onOrdersChanged(isNew: Boolean) {
        if (isNew) {
            buttonSend.visibility = View.VISIBLE
        } else {
            buttonSend.visibility = View.INVISIBLE
        }
    }
}

