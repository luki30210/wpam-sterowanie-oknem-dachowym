package pl.patro.lukasz.sterowanieoknem

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_settings.*
import org.json.JSONObject
import pl.patro.lukasz.sterowanieoknem.`interface`.AfterButtonSendClickedInterface

class HomeFragment : Fragment(), SeekBar.OnSeekBarChangeListener, AfterButtonSendClickedInterface {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(): HomeFragment = HomeFragment()
    }

    override fun afterButtonSendClicked() {
        textPercentage.text = textPercentageNew.text
        onProgressChanged(seekBarPercentage, seekBarPercentage.progress, false)
        onStopTrackingTouch(seekBarPercentage)
        openArduinoWindow(seekBarPercentage.progress*10)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        val text: String = (progress * 10).toString() + "%"
        textPercentageNew.text = text
        if (text == textPercentage.text) {
            textPercentageNew.visibility = View.INVISIBLE
        } else {
            textPercentageNew.visibility = View.VISIBLE
        }
    }

    interface OnOpenProgressChangedListener {
        fun onOpenProgressChanged(isNew: Boolean, value: Int)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {}

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        val isNew: Boolean = textPercentage.text != textPercentageNew.text
        val value: Int = textPercentageNew.text.toString().substring(0, textPercentageNew.text.length - 1).toInt()

        var mListener: OnOpenProgressChangedListener
        try {
            mListener = activity as OnOpenProgressChangedListener
            mListener.onOpenProgressChanged(isNew, value)
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement OnOpenProgressChangedListener")
        }

    }

    fun openArduinoWindow(percentage: Int) {
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val serverUrl = sharedPref.getString(getString(R.string.pref_share_ip_key), "0.0.0.0")
        val url = "http://$serverUrl:80/body"
        val queue = Volley.newRequestQueue(activity)

        var jsonBody: JSONObject = JSONObject()
        jsonBody.put("percentage", percentage)

        val jsonRequest = JsonObjectRequest(Request.Method.POST, url, jsonBody,
                Response.Listener<JSONObject> {
                    val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                    with(sharedPref.edit()) {
                        putInt(getString(R.string.home_share_open_progress), seekBarPercentage.progress)
                        commit()
                    }
                },
                Response.ErrorListener {
                    IpErrorDialog().show(fragmentManager, "IP_ERROR_DIALOG")
                }
        )
        queue.add(jsonRequest)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        seekBarPercentage.setOnSeekBarChangeListener(this)
        seekBarPercentage.progress = sharedPref.getInt(getString(R.string.home_share_open_progress), 0)
        textPercentage.text = textPercentageNew.text
        onProgressChanged(seekBarPercentage, seekBarPercentage.progress, false)
    }
}
