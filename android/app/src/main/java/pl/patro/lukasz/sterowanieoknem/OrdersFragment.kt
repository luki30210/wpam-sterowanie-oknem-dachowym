package pl.patro.lukasz.sterowanieoknem

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.SeekBar
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_orders.*
import org.json.JSONObject
import pl.patro.lukasz.sterowanieoknem.`interface`.AfterButtonSendClickedInterface
import pl.patro.lukasz.sterowanieoknem.model.Orders

class OrdersFragment : Fragment(), AfterButtonSendClickedInterface {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders, container, false)
    }

    private val onMinTempSeekBarChange = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            minTempTextView.text = (seekBar!!.progress + 11).toString() + 0x00B0.toChar() + "C"
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            emitChange()
        }
    }

    private val onMaxTempSeekBarChange = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            maxTempTextView.text = (seekBar!!.progress + 22).toString() + 0x00B0.toChar() + "C"
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            emitChange()
        }
    }

    private val autoOpenCloseSwitchChangeListener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked -> emitChange() }

    interface OnOrdersChangedListener {
        fun onOrdersChanged(isNew: Boolean)
    }

    private fun emitChange() {
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        var isNew = true
        if (sharedPref.getBoolean(getString(R.string.orders_share_auto_open_switch_key), false) == autoOpenSwitch.isChecked
                && sharedPref.getBoolean(getString(R.string.orders_share_auto_close_switch_key), false) == autoCloseSwitch.isChecked
                && sharedPref.getInt(getString(R.string.orders_share_min_temp), 0) == minTempSeekBar.progress
                && sharedPref.getInt(getString(R.string.orders_share_max_temp), 0) == maxTempSeekBar.progress) {
            isNew = false
        }

        var mListener: OnOrdersChangedListener
        try {
            mListener = activity as OnOrdersChangedListener
            mListener.onOrdersChanged(isNew)
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement OnOrdersChangedListener")
        }
    }

    override fun afterButtonSendClicked() {
        var ordersNew: Orders = Orders()
        ordersNew.autoClose = autoCloseSwitch.isChecked
        ordersNew.autoOpen = autoOpenSwitch.isChecked
        ordersNew.minTemp = minTempSeekBar.progress
        ordersNew.maxTemp = maxTempSeekBar.progress

        sendOrdersToArduino(ordersNew)
    }

    private fun updateOrders(newOrders: Orders) {
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        with(sharedPref.edit()) {
            putInt(getString(R.string.orders_share_min_temp), newOrders.minTemp)
            putInt(getString(R.string.orders_share_max_temp), newOrders.maxTemp)
            putBoolean(getString(R.string.orders_share_auto_close_switch_key), newOrders.autoClose)
            putBoolean(getString(R.string.orders_share_auto_open_switch_key), newOrders.autoOpen)
            commit()
        }
        emitChange()
    }

    fun sendOrdersToArduino(orders: Orders) {
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val serverUrl = sharedPref.getString(getString(R.string.pref_share_ip_key), "0.0.0.0")
        val url = "http://$serverUrl:80/body"
        val queue = Volley.newRequestQueue(activity)

        var jsonBody: JSONObject = JSONObject()
        jsonBody.put("autoClose", orders.autoClose)
        jsonBody.put("autoOpen", orders.autoOpen)
        jsonBody.put("minTemp", orders.minTemp + 11)
        jsonBody.put("maxTemp", orders.maxTemp + 22)

        val jsonRequest = JsonObjectRequest(Request.Method.POST, url, jsonBody,
                Response.Listener<JSONObject> {
                    updateOrders(orders)
                },
                Response.ErrorListener {
                    IpErrorDialog().show(fragmentManager, "IP_ERROR_DIALOG")
                }
        )
        queue.add(jsonRequest)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        autoOpenSwitch.setOnCheckedChangeListener(autoOpenCloseSwitchChangeListener)
        autoCloseSwitch.setOnCheckedChangeListener(autoOpenCloseSwitchChangeListener)
        minTempSeekBar.setOnSeekBarChangeListener(onMinTempSeekBarChange)
        maxTempSeekBar.setOnSeekBarChangeListener(onMaxTempSeekBarChange)

        minTempSeekBar.progress = sharedPref.getInt(getString(R.string.orders_share_min_temp), 0)   //onMinTempSeekBarChange.onProgressChanged(minTempSeekBar, sharedPref.getInt(getString(R.string.orders_share_min_temp), 0), false)
        maxTempSeekBar.progress = sharedPref.getInt(getString(R.string.orders_share_max_temp), 0)   //onMaxTempSeekBarChange.onProgressChanged(maxTempSeekBar, sharedPref.getInt(getString(R.string.orders_share_max_temp), 0), false)
        autoCloseSwitch.isChecked = sharedPref.getBoolean(getString(R.string.orders_share_auto_close_switch_key), false)
        autoOpenSwitch.isChecked = sharedPref.getBoolean(getString(R.string.orders_share_auto_open_switch_key), false)

    }

    companion object {
        @JvmStatic
        fun newInstance(): OrdersFragment = OrdersFragment()
    }
}