package pl.patro.lukasz.sterowanieoknem

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        ipAddress.setText(sharedPref.getString(getString(R.string.pref_share_ip_key), "0.0.0.0"))
        ipAddress.addTextChangedListener(onIpAddressChange)

        fireSwitch.isChecked = sharedPref.getBoolean(getString(R.string.pref_share_fire_switch_key), false)
        fireSwitch.setOnCheckedChangeListener(fireSwitchOnCheckedChangeListener)

        gasSwitch.isChecked = sharedPref.getBoolean(getString(R.string.pref_share_gas_switch_key), false)
        gasSwitch.setOnCheckedChangeListener(gasSwitchOnCheckedChangeListener)

        //ipAddressLayout.setOnClickListener(onClickTest)
    }

    companion object {
        @JvmStatic
        fun newInstance(): SettingsFragment = SettingsFragment()
    }

    private val onIpAddressChange = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
            with(sharedPref.edit()) {
                putString(getString(R.string.pref_share_ip_key), s.toString())
                commit()
            }
        }
    }

    private val fireSwitchOnCheckedChangeListener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putBoolean(getString(R.string.pref_share_fire_switch_key), isChecked)
            commit()
        }
    }

    private val gasSwitchOnCheckedChangeListener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putBoolean(getString(R.string.pref_share_gas_switch_key), isChecked)
            commit()
        }
    }

    /*private val onClickTest = View.OnClickListener {
        //IpErrorDialog().show(fragmentManager, "IP_ERROR_DIALOG")

        val sharedPref = activity.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        println(sharedPref.getString(getString(R.string.pref_share_ip_key), "0"))
    }*/
}