#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "handleBody.h"

ESP8266WebServer server(80);

const char* ssid = "ZlotyLemurBambusowy";
const char* password =  "nexus95zlb";
// const char* ssid = "UPC36F8DE9";
// const char* password =  "p44yrptvQfye1";

void setup() {

  Serial.begin(9600);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  server.on("/body", handleBody);

  server.begin();

}

void loop() {

  server.handleClient(); //Handling of incoming requests

}
