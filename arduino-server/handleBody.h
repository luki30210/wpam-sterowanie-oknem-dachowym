extern ESP8266WebServer server;

void handleBody() {

  if (server.hasArg("plain") == false) {
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = server.arg("plain");
  Serial.println(message);
  
  int counter = 0;
  while (Serial.available() < 1) {
    delay(10);
    counter++;
    if (counter > 500) {
      server.send(500, "text/plain", "No response from Arduino :(");
      return;
    }
  }

  if (Serial.available() > 0) {
    String readedData = Serial.readString();
    server.send(200, "text/plain", readedData);
  }

}
